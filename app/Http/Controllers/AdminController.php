<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    //Logout Method Using Guard Web
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/login');
    }


    public function profile()
    {
        //check the id from log in user
        $id = Auth::user()->id;

        //get the user based on the $id
        $adminData = User::find($id);

        return view('admin.profile', compact('adminData'));
    }

    public function updateProfile(Request $request)
    {
        //check the id from log in user
        $id = Auth::user()->id;
        //get the user based on the $id
        $data = User::find($id);

        $request->validate([
            'name' => 'required|max:100',
            'username' => 'required|max:20|unique:users,username'
        ],[
            'name.required' => 'Nama Lengkap Pengguna Harus Terisi',
            'username.unique' => 'Username Sudah Ada',
            'username.required' => 'Username Harus Terisi'
        ]);

        if($request->hasFile('profile_img')){
            $request->validate([
                'profile_img' => 'image|mimes:jpg,jpeg,png|max:2048'
            ], [
                'profile_img.image' => 'File Harus Berupa Gambar Dengan Format jpg, jpeg, png',
                'profile_img.max' => 'File Tidak Boleh Lebih Dari 2 mb'
            ]);
            
            if(!empty($data->profile_img)){
                //remove the file image from public folder
                unlink(public_path('uploads/admin/'.$data->profile_img));
            }
            
            //get the extension of image from request
            $ext = $request->file('profile_img')->extension();

            //name the uploaded image
            $img_name = 'profile_img'.'.'.$ext;

            //moved image to public folder with its name
            $request->file('profile_img')->move(public_path('uploads/admin/'), $img_name);

            //update the image in database
            $data->profile_img = $img_name;
        }

        $data->name = $request->name;
        $data->username = $request->username;
        $data->update();

        $notif= array(
            'message' => 'Data Berhasil Diperbarui',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.profile')->with($notif);
    }

    public function changePass(){

        return view('admin.changePass');

    }

    public function updatePass(Request $request){

        // validate request for input
        $validateData = $request->validate([
            'old_pass' => 'required',
            'new_pass' => 'required',
            'confir_pass' => 'required|same:new-pass'
        ],[
            'old_pass.required' => 'Kata Sandi Lama Harus Terisi',
            'new_pass.required' => 'Kata Sandi Baru Harus Terisi',
            'confir_pass.required' => 'Konfirmasi Kata Sandi Harus Terisi',
            'confir_pass.same' => 'Konfirmasi Kata Sandi Tidak Cocok'
        ]);

        //take hashed password from authenticated user
        $hashPass = Auth::user()->password;
        
        //check for old_pass request, if its equal to hashed password in database.
        if(Hash::check($request->old_pass, $hashPass)){

            $userPass = User::find(Auth::id()); //find the id of the user
            $userPass->password = bcrypt($request->new_pass); //hashed the new password that from user request
            $userPass->save(); //save that password

            session()->flash('message', 'Kata Sandi Berhasil Diperbarui');
            return redirect()->back();
        }else{
            session()->flash('message', 'Kata Sandi Lama Tidak Sesuai');
            return redirect()->back();
        }

    }
}
