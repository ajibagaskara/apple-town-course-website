@extends('admin.master')
@section('content')
    {{-- jquery for image preview --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    
    {{-- update profile form --}}
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Profile</h4>
                                <p class="card-title-desc">Silahkan Ubah Data Sesuai Kebutuhan</p>
                                <hr>
                                {{-- error message from validation --}}
                                @if (count($errors))
                                    @foreach($errors->all() as $error)
                                        <p class="alert alert-danger alert-dismissible fade show mb-1">{{$error}}</p>
                                    @endforeach
                                @endif
                                <form action="{{ route('update.profile') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                                    @csrf
                                    <div class="row mb-3">
                                        <label for="name-input" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="text" value="{{ $adminData->name }}" name="name" id="name-input">
                                        </div>
                                    </div>
                                    <!-- end row -->
                                    <div class="row mb-3">
                                        <label for="username-input" class="col-sm-2 col-form-label"><em>Username</em></label>
                                        <div class="col-sm-10">
                                            <input class="form-control" type="search" value="{{ $adminData->username }}" name="username" id="username-input">
                                        </div>
                                    </div>
                                    <!-- end row -->
                                    <div class="input-group">
                                        <label for="img-input" class="col-sm-2 col-form-label">Ganti Gambar</label>
                                        <div class="col-sm-8">
                                            <input type="file" name="profile_img" class="form-control" id="img-input">
                                        </div>
                                        <button onClick="window.location.reload();" class="btn btn-sm btn-outline-danger waves-effect waves-light">Hapus Link Gambar</button>
                                    </div>
                                    <hr>
                                    <input type="submit" class="btn btn-sm btn-outline-success waves-effect waves-light" value="Perbarui">        
                                </form>
                            </div>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-4">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <h4 class="card-title">Gambar Profile</h4>
                                    <img src="{{ (!empty($adminData->profile_img))? url('uploads/admin/'.$adminData->profile_img):url('uploads/no-image.png') }}" class="img-fluid" alt="Responsive image" id="img-show">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- image preview script --}}
    <script type="text/javascript">
        $(document).ready(function(){
            $('#img-input').change(function(e){
                var reader = new FileReader();
                reader.onload = function(e){
                    $('#img-show').attr('src',e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });
    </script>

@endsection