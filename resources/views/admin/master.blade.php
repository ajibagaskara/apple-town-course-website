<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Dashboard | Apple Town Course Samarinda</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Apple Town Course Samarinda Admin Panel Website" name="description" />

        <!-- CSS -->
        @include('admin.partials.style')

    </head>

    <body data-topbar="dark">
    
    <!-- <body data-layout="horizontal" data-topbar="dark"> -->
        <!-- Begin page -->
        <div id="layout-wrapper">

            <!-- ========== Header Start ========== -->
            @include('admin.partials.header')
            <!-- Header End -->

            <!-- ========== Left Sidebar Start ========== -->
            @include('admin.partials.sidebar')
            <!-- Left Sidebar End -->
  
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            @yield('content')
            <!-- end main content-->

        </div>
        <!-- END layout-wrapper -->

        <!-- JAVASCRIPT -->
        @include('admin.partials.script')
    </body>

</html>