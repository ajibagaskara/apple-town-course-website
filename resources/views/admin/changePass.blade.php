@extends('admin.master')
@section('content')
{{-- update password form --}}
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Ubah Kata Sandi</h4>
                            <hr>

                            {{-- error message from validation --}}
                            @if (count($errors))
                                @foreach($errors->all() as $error)
                                    <p class="alert alert-danger alert-dismissible fade show mb-1">{{$error}}</p>
                                @endforeach
                            @endif

                            <form action="{{ route('update.pass')}}" method="POST">
                                @csrf
                                <div class="row mb-3">
                                    <label for="old_pass" class="col-sm-2 col-form-label">Kata Sandi Lama</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="password" value=""
                                        id="old_pass" name="old_pass">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="new_pass" class="col-sm-2 col-form-label">Kata Sandi Baru</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="password" id="new_pass" name="new_pass">
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="confir_pass" class="col-sm-2 col-form-label">Konfirmasi Kata Sandi</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="password" id="confir_pass" name="confir_pass">
                                    </div>
                                </div>
                                <hr>
                                <input type="submit" class="btn btn-sm btn-outline-success waves-effect waves-light" value="Perbarui">        
                            </form>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>
@endsection