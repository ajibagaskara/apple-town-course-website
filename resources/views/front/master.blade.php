<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="{{ asset('frontend/assets/img/favicon.png')}}">
    @include('front.partials.style')
</head>

<body>
    <!--::header part start::-->
    @include('front.partials.header')
    <!-- Header part end-->

    <!-- banner part start-->
    @include('front.partials.banner')
    <!-- banner part start-->

    <!-- feature_part start-->
    @include('front.partials.feature')
    <!-- upcoming_event part start-->

    <!-- learning part start-->
    @include('front.partials.learning')
    <!-- learning part end-->

    <!-- member_counter counter start -->
    @include('front.partials.member')
    <!-- member_counter counter end -->

    <!--::blog_part start::-->
    @yield('content')
    <!--::blog_part end::-->

    <!-- footer part start-->
    @include('front.partials.footer')
    <!-- footer part end-->

    <!-- jquery plugins here-->
    @include('front.partials.script')
</body>

</html>