<!-- jquery -->
<script src="{{ asset('frontend/assets/js/jquery-1.12.1.min.js')}}"></script>
<!-- popper js -->
<script src="{{ asset('frontend/assets/js/popper.min.js')}}"></script>
<!-- bootstrap js -->
<script src="{{ asset('frontend/assets/js/bootstrap.min.js')}}"></script>
<!-- easing js -->
<script src="{{ asset('frontend/assets/js/jquery.magnific-popup.js')}}"></script>
<!-- swiper js -->
<script src="{{ asset('frontend/assets/js/swiper.min.js')}}"></script>
<!-- swiper js -->
<script src="{{ asset('frontend/assets/js/masonry.pkgd.js')}}"></script>
<!-- particles js -->
<script src="{{ asset('frontend/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{ asset('frontend/assets/js/jquery.nice-select.min.js')}}"></script>
<!-- swiper js -->
<script src="{{ asset('frontend/assets/js/slick.min.js')}}"></script>
<script src="{{ asset('frontend/assets/js/jquery.counterup.min.js')}}"></script>
<script src="{{ asset('frontend/assets/js/waypoints.min.js')}}"></script>
<!-- custom js -->
<script src="{{ asset('frontend/assets/js/custom.js')}}"></script>