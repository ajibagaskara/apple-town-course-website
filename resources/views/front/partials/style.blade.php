<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('frontend/assets/css/bootstrap.min.css')}}">
<!-- animate CSS -->
<link rel="stylesheet" href="{{ asset('frontend/assets/css/animate.css')}}">
<!-- owl carousel CSS -->
<link rel="stylesheet" href="{{ asset('frontend/assets/css/owl.carousel.min.css')}}">
<!-- themify CSS -->
<link rel="stylesheet" href="{{ asset('frontend/assets/css/themify-icons.css')}}">
<!-- flaticon CSS -->
<link rel="stylesheet" href="{{ asset('frontend/assets/css/flaticon.css')}}">
<!-- font awesome CSS -->
<link rel="stylesheet" href="{{ asset('frontend/assets/css/magnific-popup.css')}}">
<!-- swiper CSS -->
<link rel="stylesheet" href="{{ asset('frontend/assets/css/slick.css')}}">
<!-- style CSS -->
<link rel="stylesheet" href="{{ asset('frontend/assets/css/style.css')}}">